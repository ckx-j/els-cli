const addWeb = require('../lib/addWeb')

const run = ({pro, isSsr}) => {
  addWeb({pro, isSsr})
}

module.exports = run