#!/usr/bin/env node

const run = require('./index.js')
const argvs = process.argv
const server = require('../utils/server')
if (argvs[2] === 'add-web') {
  server.addWeb({}, ({pro, isSsr}) => {
    run[argvs[2]]({pro, isSsr})
  })
} else {
  server.select({add: argvs[2] === 'dev-add'}, ({pro, isSsr}) => {
    run[argvs[2]]({pro, isSsr})
  })
}



