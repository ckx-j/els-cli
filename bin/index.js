const dev = require('./dev')
const devAdd = require('./dev-add')
const build = require('./build')
const addWeb = require('./add-web')
module.exports = {
  dev,
  build,
  'dev-add': devAdd,
  'add-web': addWeb
}