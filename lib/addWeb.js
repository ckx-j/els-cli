const fs = require('fs')
const path = require('path')
const method = require('../utils/method')
const rootPth = process.env.INIT_CWD

module.exports = ({pro, isSsr}) => {
  method.createFile(path.join(rootPth, `web/${pro}`))
  method.setMouldFiles({pro, isSsr})
  const settingPath = path.join(rootPth, '.els/proList.json')
  method.createFile(settingPath)
  let config = {}
  if (fs.existsSync(settingPath)) {
    config = require(settingPath)
  }
  config[pro] = {
    ssr: isSsr
  }
  fs.writeFileSync(settingPath, JSON.stringify(config, null, 2))
}