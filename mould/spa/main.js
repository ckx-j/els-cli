import { createApp } from 'vue'
import { createWebHistory } from 'vue-router'
import createRouter from './router'
import App from './App.vue'

const app = createApp(App)
const router = createRouter(createWebHistory('<-- pro !->'))
app.use(router)

app.mount('#app')