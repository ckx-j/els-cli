import { createRouter } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../views/home.vue')
  }
]

export default function (history) {
  return createRouter({
    history,
    routes
  })
}