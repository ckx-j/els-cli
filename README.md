基于vue3.0+，可选打包模式服务端渲染和客户端渲染；按项目打包，web根目录下文件是一个单独的项目，可自定义打包配置。

添加一个前端项目   

```
npm run add:web  
```
  
启动   

```
npm run dev
```

    
增加一个前端项目的运行  

```
npm run dev:add
```
  
    
打包  

```
npm run build
```

   
demo: https://gitee.com/ckx-j/v3-egg-demo.git  
学习交流qq群：546874834  
微信号：els_cms
